﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Arkanoid {
    public class InterfaceManager : MonoBehaviour {
        [SerializeField, Tooltip("Надписи, показывающие количество жизней")]
        private Text _healthCount1, _healthCount2;
		[SerializeField, Tooltip("Надписи, показывающие количество блоков")]
        private Text _blocksCount1, _blocksCount2;

		private void Update() {
			_healthCount1.text = GameManager.playerHealth.ToString();
			_healthCount2.text = GameManager.playerHealth.ToString();
			_blocksCount1.text = GameManager.BlocksCount.ToString();
			_blocksCount2.text = GameManager.BlocksCount.ToString();
		}
	}
}