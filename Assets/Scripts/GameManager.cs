﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Arkanoid {
    public class GameManager : MonoBehaviour {
        [SerializeField, Tooltip("Количество жизней игроков")]
        private int PlayerHealth;
        [SerializeField, Tooltip("Родительский объект игроков и шара")]
        private GameObject _playersAndBall;
        [SerializeField, Tooltip("Массив всех тоннелей по порядку")]
        private GameObject[] _tonnels;
        public static int playerHealth;
		public static int BlocksCount;
		private int _levelNumber = 0, _maxLevel;

		private void Start() {
			playerHealth = PlayerHealth;
			_tonnels = _tonnels.Where(t => t != null).ToArray<GameObject>();
			_maxLevel = _tonnels.Length;
			BlocksCount = _tonnels[_levelNumber].transform.Find("Blocks").childCount;
		}
		public static void CheckLose() {
			if(playerHealth <= 0) {
                Time.timeScale = 0;
                Debug.Log("Вы проиграли, потому что закончились жизни!");
			}
		}
		public void CheckWin() {
			if(BlocksCount <= 0) {
				Debug.Log("Ура, Вы прошли уровень!");

				_levelNumber++;

				if(_levelNumber < _maxLevel) {
					NextLevel();
				}
				else {
					Debug.Log("!!!!!Ура, Вы прошлю всю игру!!!!!");
				}
			}
		}
		public void NextLevel() {
			BlocksCount = _tonnels[_levelNumber].transform.Find("Blocks").childCount;
			var offset = 100f;
			_playersAndBall.transform.position += new Vector3(offset, 0f, 0f);
			var ballComponent = _playersAndBall.transform.GetComponentInChildren<BallComponent>();
			ballComponent.SetAttached(true);
			ballComponent.SetCurrentPlayer(_playersAndBall.transform.GetChild(0).gameObject);
		}
    }
}