﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid {
	public class PlayerComponent : MonoBehaviour {
		[SerializeField, Tooltip("Скорость игрока")]
		private float _speed;
		private Vector3 _inertia;
		private float _timeinertia;
		[SerializeField, Tooltip("Значения для ограничения перемещения игроков")]
		private float maxValueX, maxValueY;
		private float offset;

		public void AddMaxValueX(float value) {
			maxValueX += value;
		}

		private void Start() {
			offset = maxValueX * 2f;
		}

		private void Update() {
			if(_timeinertia > 0) {
				_timeinertia -= Time.deltaTime;
			}
			if(_timeinertia < 0) {
				_timeinertia = 0;
			}
		}

		private void FixedUpdate() {
			if(transform.localPosition.x > maxValueX) {
				transform.localPosition = new Vector3(maxValueX, transform.localPosition.y, transform.localPosition.z);
			}
			if(transform.localPosition.x < -maxValueX) {
				transform.localPosition = new Vector3(-maxValueX, transform.localPosition.y, transform.localPosition.z);
			}
			if(transform.localPosition.y > maxValueY) {
				transform.localPosition = new Vector3(transform.localPosition.x, maxValueY, transform.localPosition.z);
			}
			if(transform.localPosition.y < -maxValueY) {
				transform.localPosition = new Vector3(transform.localPosition.x, -maxValueY, transform.localPosition.z);
			}

			if(name == "Player1") {
				if(Input.GetKey(KeyCode.W)) {
					Move("up");
				}
				if(Input.GetKey(KeyCode.S)) {
					Move("down");
				}
				if(Input.GetKey(KeyCode.D)) {
					Move("left");
				}
				if(Input.GetKey(KeyCode.A)) {
					Move("right");
				}
			}
			else {
				if(Input.GetKey(KeyCode.UpArrow)) {
					Move("up");
				}
				if(Input.GetKey(KeyCode.DownArrow)) {
					Move("down");
				}
				if(Input.GetKey(KeyCode.RightArrow)) {
					Move("left");
				}
				if(Input.GetKey(KeyCode.LeftArrow)) {
					Move("right");
				}
			}
			if(_timeinertia > 0) {
				Inertia();
			}
		}

		private void Move(string dir) {
			_timeinertia = 1f;
			switch(dir) {
				case "up":
					transform.position += transform.up * _speed * Time.deltaTime;
					_inertia = transform.up * _timeinertia * Time.deltaTime;
					break;
				case "down":
					transform.position += -transform.up * _speed * Time.deltaTime;
					_inertia = -transform.up * _timeinertia * Time.deltaTime;
					break;
				case "left":
					if(name == "Player1") {
						transform.position += transform.right * _speed * Time.deltaTime;
						_inertia = transform.right * _timeinertia * Time.deltaTime;
					}
					else {
						transform.position += transform.right * _speed * Time.deltaTime;
						_inertia = transform.right * _timeinertia * Time.deltaTime;
					}
					break;
				case "right":
					if(name == "Player1") {
						transform.position += -transform.right * _speed * Time.deltaTime;
						_inertia = -transform.right * _timeinertia * Time.deltaTime;
					}
					else {
						transform.position += -transform.right * _speed * Time.deltaTime;
						_inertia = -transform.right * _timeinertia * Time.deltaTime;
					}
					break;
			}
		}

		private void Inertia() {
			transform.position += _inertia;
		}
	}
}