﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid {
	public class BlockComponent : MonoBehaviour {
		[SerializeField]
		private Color[] _colors;
		[SerializeField]
		private GameObject _gameManager;

		private void Start() {
			_gameManager = GameObject.Find("GameManager");
			transform.rotation = Quaternion.Euler(Random.Range(0f, 100f), Random.Range(0f, 100f), Random.Range(0f, 100f));
			GetComponent<MeshRenderer>().material.color = _colors[Random.Range(0, 5)];
		}
		private void OnCollisionEnter(Collision collision) {
			if(collision.gameObject.name == "Ball") {
				Destroy(gameObject);
				GameManager.BlocksCount--;
				_gameManager.GetComponent<GameManager>().CheckWin();
			}
		}
	}
}