﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Arkanoid {
	public class MenuManager : MonoBehaviour {
		[SerializeField]
		private Text _versionText;
		[SerializeField]
		private GameObject _menuPause, _settingsPanel, _mainMenuPanel;
		[SerializeField]
		private Toggle _soundOff, _easy, _medium, _hard;
		[SerializeField]
		private Slider _volume;

		private void Start() {
			if(_versionText != null) {
				_versionText.text = "ver: " + Application.version;
			}
			if(_mainMenuPanel != null) {
				_mainMenuPanel.GetComponent<Animation>().Play("MainMenu");
			}
		}
		private void Update() {
			if(_menuPause != null && Input.GetKeyDown(KeyCode.Escape)) {
				if(!_menuPause.activeSelf) {
					_menuPause.SetActive(true);
					_menuPause.GetComponent<Animation>().Play("PauseMenuOpen");
					StartCoroutine(PauseMenuOpenAfterAnim());
				}
				else {
					_menuPause.GetComponent<Animation>().Play("PauseMenuClose");
					StartCoroutine(PauseMenuCloseAfterAnim());
				}
			}
		}
		IEnumerator PauseMenuOpenAfterAnim() {
			yield return new WaitForSeconds(_menuPause.GetComponent<Animation>().GetClip("PauseMenuOpen").length);
			Time.timeScale = 0f;
		}
		IEnumerator PauseMenuCloseAfterAnim() {
			yield return new WaitForSeconds(_menuPause.GetComponent<Animation>().GetClip("PauseMenuClose").length);
			_menuPause.SetActive(false);
			Time.timeScale = 1f;
		}
		public void NewGame() {
			SceneManager.LoadScene("Game");
		}
		public void ExitGame() {
			if(Application.isEditor) {
				EditorApplication.isPlaying = false;
			}
			else {
				Application.Quit();
			}
		}
		public void RestartGame() {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			Time.timeScale = 1f;
		}
		public void ResumeGame() {
			Time.timeScale = 1f;
			_menuPause.GetComponent<Animation>().Play("PauseMenuClose");
			StartCoroutine(PauseMenuCloseAfterAnim());
		}
		public void OpenSettings() {
			if(!_settingsPanel.activeSelf) {
				_mainMenuPanel.GetComponent<Animation>().Play("MainMenuClose");
				StartCoroutine(OpenSettingsAnim(_mainMenuPanel, "MainMenuClose"));
				GetSettings();
			}
			else {
				_settingsPanel.GetComponent<Animation>().Play("SettingsMenuClose");
				StartCoroutine(OpenMainMenuAnim(_mainMenuPanel, "MainMenu"));
			}
		}
		private void GetSettings() {
			switch(PlayerPrefs.GetString("SoundOff")) {
				case "on":
					_soundOff.isOn = true;
					break;
				case "off":
					_soundOff.isOn = false;
					break;
			}

			_volume.value = PlayerPrefs.GetFloat("Volume");

			switch(PlayerPrefs.GetString("Complexity")) {
				case "Easy":
					_easy.isOn = true;
					_medium.isOn = false;
					_hard.isOn = false;
					break;
				case "Medium":
					_medium.isOn = true;
					_easy.isOn = false;
					_hard.isOn = false;
					break;
				case "Hard":
					_hard.isOn = true;
					_easy.isOn = false;
					_medium.isOn = false;
					break;
				default:
					_easy.isOn = true;
					_medium.isOn = false;
					_hard.isOn = false;
					break;
			}
		}
		public void OnValueChangetEasy() {
			if(_easy.isOn) {
				PlayerPrefs.SetString("Complexity", "Easy");
				_medium.isOn = false;
				_hard.isOn = false;
			}
		}
		public void OnValueChangetMedium() {
			if(_medium.isOn) {
				PlayerPrefs.SetString("Complexity", "Medium");
				_easy.isOn = false;
				_hard.isOn = false;
			}
		}
		public void OnValueChangetHard() {
			if(_hard.isOn) {
				PlayerPrefs.SetString("Complexity", "Hard");
				_easy.isOn = false;
				_medium.isOn = false;
			}
		}
		public void OnValueChangetSoundOff() {
			if(_soundOff.isOn) {
				PlayerPrefs.SetString("SoundOff", "on");
			}
			else {
				PlayerPrefs.SetString("SoundOff", "off");
			}
		}
		public void OnValueChangetVolume() {
			PlayerPrefs.SetFloat("Volume", _volume.value);
		}
		IEnumerator OpenSettingsAnim(GameObject menu, string nameAnim) {
			while(menu.GetComponent<Animation>().IsPlaying(nameAnim)) {
				yield return null;
			}
			_settingsPanel.SetActive(true);
			_settingsPanel.GetComponent<Animation>().Play("SettingsMenuOpen");
		}
		IEnumerator OpenMainMenuAnim(GameObject menu, string nameAnim) {
			while(_settingsPanel.GetComponent<Animation>().IsPlaying("SettingsMenuClose")) {
				yield return null;
			}
			_settingsPanel.SetActive(false);
			menu.GetComponent<Animation>().Play(nameAnim);
		}
	}
}