﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid {
	public class BallComponent : MonoBehaviour {
		[Tooltip("Игровой объект игроков")]
		public GameObject Player1, Player2;
		[SerializeField, Tooltip("Скорость шара")]
		private float Speed;
		[SerializeField, Tooltip("Максимальная скорость шара")]
		private float MaxSpeed;
		private float _speed;
		private bool _attached = true;
		private GameObject _currentPlayer;
		private Vector3 _velocity;

		public void SetAttached(bool value) {
			if(value) {
				_attached = value;
			}
		}
		public void SetCurrentPlayer(GameObject player) {
			_currentPlayer = player;
		}
		private void Start() {
			_currentPlayer = Player1;
			_velocity = transform.forward;
			_speed = Speed;
		}
		private void Update() {
			if(Input.GetKeyDown(KeyCode.Space) && _attached) {
				_attached = false;
				_currentPlayer = null;
			}
		}
		private void FixedUpdate() {
			if(_attached && _currentPlayer != null) {
				transform.rotation = _currentPlayer.transform.rotation;
				if(_currentPlayer.name == "Player1") {
					transform.position = new Vector3(_currentPlayer.transform.position.x, _currentPlayer.transform.position.y, -47.61f);
				}
				else {
					transform.position = new Vector3(_currentPlayer.transform.position.x, _currentPlayer.transform.position.y, 47.61f);
				}
				_velocity = transform.forward;
				_speed = Speed;
			}
			else {
				transform.position += _velocity * _speed * Time.deltaTime;
			}
		}
		private void OnCollisionEnter(Collision collision) {
			_velocity = Vector3.Reflect(_velocity, collision.contacts[0].normal);
			if(_speed < MaxSpeed && collision.gameObject.GetComponent<BlockComponent>() != null) {
				_speed += 1;
			}
		}
	}
}