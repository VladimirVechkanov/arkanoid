﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid {
	public class GateComponent : MonoBehaviour {
		private void OnTriggerEnter(Collider other) {
			GameManager.playerHealth -= 1;
			GameManager.CheckLose();
			var ballComponent = other.GetComponent<BallComponent>();
			if(ballComponent != null) {
				ballComponent.SetAttached(true);
				ballComponent.SetCurrentPlayer(transform.parent.gameObject);
			}
		}
	}
}